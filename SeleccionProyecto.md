**Pagina web: Catálogo de publicidad local.**

**Equipo:**
- María Saarayim Gonzalez Hernandez
- Dulce Regina Saavedra Mata


**NECESIDAD**

Gracias a la nueva normalidad, las empresas y principalmente los pequeños
comerciantes han tenido que adaptarse a la forma de trabajo, puesto que han
sufrido bajas en sus ventas, y algunos lugares han llegado a cerrar sus puertas al
público. Por tal motivo, se han visto obligados a mudar sus tiendas físicas a las
ventas en línea, pero al no poder costearse el desarrollo de una página web, sus
productos se han promovido por varias redes sociales, en las cuales se llegan a
perder los post de su información entre tantas otras publicaciones. Al igual que los
eventos culturales aunque se han hecho de manera virtual también han
necesitado publicidad.
Así mismo, se sabe que un factor de la población es de alto riesgo al virus,
por lo que es importante que se resguarden en sus hogares el mayor tiempo
posible. También cabe mencionar que ante la paranoia colectiva, se ha generado un desabasto de
medicamentos, obligando a la población a desplazarse por varias farmacias
buscando el producto deseado, elevando el riesgo de contagio.

**VENTAJAS:**

- Se encuentran productos con mayor facilidad
- Encontrar negocios separados por categorías
- Proporciona toda la información deseada para el cliente
- Cuenta con la ubicación en mapa de los lugares físicos
- Consultar eventos cercanos que se llevarán a cabo de manera virtual.
- Consultar servicios como cursos o clases que se ofrecen de manera
- remota.
- Es fácil la búsqueda de consultorios médicos
- Se pueden rastrear medicamentos sin la necesidad de salir de casa
- Se puede consultar el precio de los medicamentos de diferentes farmacias.

