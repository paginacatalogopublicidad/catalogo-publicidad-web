<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class NegociosTable extends Table
{
    public function initialize(array $config)
    {
      $this->table('negocios');
      $this->displayField('nombre');
      $this->primaryKey('id');

      $this->belongsTo('Categorias', [
            'foreignKey' => 'categoria_id',
        ]);
      $this->addBehavior('Josegonzalez/Upload.Upload', [
            'logo' => [
                'fields' => [
                    'dir' => 'logo_dir',
                    'size' => 'logo_size',
                    'type' => 'logo_type'
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    return strtolower($data['name']);
                },
                'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);

                    // Store the thumbnail in a temporary file
                    $tmp = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;

                    // Use the Imagine library to DO THE THING
                    $size = new \Imagine\Image\Box(40, 40);
                    $mode = \Imagine\Image\ImageInterface::THUMBNAIL_INSET;
                    $imagine = new \Imagine\Gd\Imagine();

                    // Save that modified file to our temp file
                    $imagine->open($data['tmp_name'])
                        ->thumbnail($size, $mode)
                        ->save($tmp);

                    // Now return the original *and* the thumbnail
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumbnail-' . $data['name'],
                    ];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    // When deleting the entity, both the original and the thumbnail will be removed
                    // when keepFilesOnDelete is set to false
                    return [
                        $path . $entity->{$field},
                        $path . 'thumbnail-' . $entity->{$field}
                    ];
                },
                'keepFilesOnDelete' => false
            ]
        ]);

    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('nombre')
            ->notEmpty('direccion')
            ->notEmpty('horario')
            ->notEmpty('telefono');

        return $validator;
    }

    public function isOwnedBy($negocioId, $userId)
    {
        return $this->exists(['id' => $negocioId, 'user_id' => $negocioId]);
    }

}
