<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{

    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('username', 'Nombre de usuario es un campo requerido')
            ->notEmpty('password', 'Contraseña es un campo requerido')
            ->notEmpty('role', 'El rol es un campo requerido')
            ->add('role', 'inList', [
                'rule' => ['inList', ['admin', 'lector']],
                'message' => 'Ingresa un rol valido'
            ]);
    }

}
