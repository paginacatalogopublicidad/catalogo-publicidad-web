<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Categoria Entity
 *
 * @property int $parent_id
 * @property int|null $lft
 * @property int|null $rght
 * @property string|null $nombre
 * @property \Cake\I18n\FrozenTime|null $creada
 * @property \Cake\I18n\FrozenTime|null $modificada
 *
 * @property \App\Model\Entity\ChildCategoria[] $child_categorias
 * @property \App\Model\Entity\Negocio[] $negocios
 */
class Categoria extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'lft' => true,
        'rght' => true,
        'nombre' => true,
        'creada' => true,
        'modificada' => true,
        'child_categorias' => true,
        'negocios' => true,
    ];
}
