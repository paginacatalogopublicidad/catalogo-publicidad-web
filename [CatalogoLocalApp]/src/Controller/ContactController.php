<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Form\ContactForm;

class ContactController extends AppController
{
    public function index()
    {
        $contact = new ContactForm();
        if ($this->request->is('post')) {
            if ($contact->execute($this->request->getData())) {
                $this->Flash->success('Nos pondremos en contacto contigo.');
            } else {
                $this->Flash->error('Ocurrió un problema con el form.');
            }
        }

        if ($this->request->is('get')) {
            $this->request->data('name', 'John Doe');
            $this->request->data('email','john.doe@example.com');
        }
        $this->set('contact', $contact);
    }
}
