<?php
namespace App\Controller;

class NegociosController extends AppController
{
  public $components = ['Flash'];

    public function index()
    {
        $negocios = $this->Negocios->find('all');
        $this->set(compact('negocios'));

    }

    public function index2()
    {
        $negocios = $this->Negocios->find('all');
        $this->set(compact('negocios'));

    }

    public function search(){
      /*$query = $this->find('all')
        ->where(['nombre LIKE' => '%' . $this->request->query('q') . '%']);


      $this->set(compact('negocios', $this->paginate($query)));

        if(isset($_POST['search']))
        {
            $valueToSearch = $_POST['valueToSearch'];
            // search in all table columns
            // using concat mysql function
            $query = "SELECT * FROM `users` WHERE CONCAT(`id`, `fname`, `lname`, `age`) LIKE '%".$valueToSearch."%'";
            $search_result = filterTable($query);

        }
         else {
            $query = "SELECT * FROM `users`";
            $search_result = filterTable($query);
        }

        // function to connect and execute the query
        function filterTable($query)
        {
            $connect = mysqli_connect("localhost", "root", "", "test_db");
            $filter_Result = mysqli_query($connect, $query);
            return $filter_Result;
        }*/
    }

    public function view($id = null)
    {
        $negocio = $this->Negocios->get($id);
        $this->set(compact('negocio'));
    }

    public function add()
    {
        $negocio = $this->Negocios->newEntity();
        if ($this->request->is('post')) {

            $negocio = $this->Negocios->patchEntity($negocio, $this->request->getData());
            $negocio->user_id = $this->Auth->user('id');
            if ($this->Negocios->save($negocio)) {
                $this->Flash->success(__('Se ha registrado el negocio correctamente.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se registró el negocio.'));
        }
        $this->set('negocio', $negocio);

        // Just added the categories list to be able to choose
        // one category for an article
        $categorias = $this->Negocios->Categorias->find('treeList');
        $this->set(compact('categorias'));

    }

    public function edit($id = null)
    {
        $negocio = $this->Negocios->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Negocios->patchEntity($negocio, $this->request->getData());
            if ($this->Negocios->save($negocio)) {
                $this->Flash->success(__('El negocio ha sido actualizado'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El negocio no se ha podido actualizar.'));
        }

        $this->set('negocio', $negocio);

        // Just added the categories list to be able to choose
        // one category for an article
        $categorias = $this->Negocios->Categorias->find('treeList');
        $this->set(compact('categorias'));
    }

    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);

        $negocio = $this->Negocios->get($id);
        if ($this->Negocios->delete($negocio)) {
            $this->Flash->success(__('El negocio con id: {0} ha sido eliminado.', h($id)));
            return $this->redirect(['action' => 'index2']);
        }
    }

    public function isAuthorized($user)
    {
        // All registered users can add articles
        if ($this->request->getParam('action') === 'add') {
            return true;
        }

        // The owner of an article can edit and delete it
        if (in_array($this->request->getParam('action'), ['edit', 'delete'])) {
            $negocioId = (int)$this->request->getParam('pass.0');
            if ($this->Negocios->isOwnedBy($negocioId, $user['id'])) {
                return true;
            }
        }

        return parent::isAuthorized($user);
    }



}
