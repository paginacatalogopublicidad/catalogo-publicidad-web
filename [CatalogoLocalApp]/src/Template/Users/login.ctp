<!-- File: src/Template/Users/login.ctp -->
<div class="row container-negocio">
  <div class="col-md-4">&nbsp;&nbsp;</div>
  <div class="col-md-4 contenido-negocio">
    <div class="users form">
    <?= $this->Flash->render('auth') ?>
    <?= $this->Form->create() ?>
        <fieldset>
            <legend><?= __('Ingrese usuario y contraseña') ?></legend>
            <?= $this->Form->input('username') ?>
            <?= $this->Form->input('password') ?>
        </fieldset>
    <?= $this->Form->button(__('Ingresar')); ?>
    <?= $this->Form->end() ?>
    </div>
  </div>

  <div class="col-md-4">&nbsp;&nbsp;</div>
</div>  
