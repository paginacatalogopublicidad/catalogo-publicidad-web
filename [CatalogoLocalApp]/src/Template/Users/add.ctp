<!-- src/Template/Users/add.ctp -->
<head>

  <script>
  function passwordChanged() {
      var strength = document.getElementById('strength');
      var strongRegex = new RegExp("^(?=.{14,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
      var mediumRegex = new RegExp("^(?=.{10,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
      var enoughRegex = new RegExp("(?=.{8,}).*", "g");
      var pwd = document.getElementById("password");
      if (pwd.value.length == 0) {
          strength.innerHTML = 'Type Password';
      } else if (false == enoughRegex.test(pwd.value)) {
          strength.innerHTML = 'More Characters';
      } else if (strongRegex.test(pwd.value)) {
          strength.innerHTML = '<span style="color:green">Strong!</span>';
      } else if (mediumRegex.test(pwd.value)) {
          strength.innerHTML = '<span style="color:orange">Medium!</span>';
      } else {
          strength.innerHTML = '<span style="color:red">Weak!</span>';
      }
  }
  </script>
</head>
<div class="users form">
<?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Registrar Usuario') ?></legend>
        <?= $this->Form->input('username', array('maxlength'=>'40')) ?>
        <?= $this->Form->input('password', array('maxlength'=>'30', 'onKeyUp'=>'passwordChanged()')) ?>
        <span id="strength">Type Password</span>


        <?= $this->Form->input('role', [
            'options' => ['admin' => 'Admin', 'lector' => 'Lector']
        ]) ?>
   </fieldset>
<?= $this->Form->button(__('Registrar')); ?>
<?= $this->Form->end() ?>

</div>
