<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Categoria $categoria
 */
?>

<div class="row">
  <h1 class="TitulosNegocio"><?= h($categoria->nombre) ?></h1>


    <div class="col-md-4 columna-central">

        <?php if (!empty($categoria->negocios)): ?>
          <table class="table table-striped table-class tabla-Negocios" id="tablaNegocios">

          <tbody>
              <?php foreach ($categoria->negocios as $negocios): ?>

              <tr>

                  <td>

                      <img class="ImagenGrid" src="http://localhost:8080/catalogo-publicidad-web/[CatalogoLocalApp]/webroot/files/Negocios/logo/<?= h($negocios->logo) ?>" alt="Imagen Negocio">
                      <br />
                      <?= $this->Html->link($negocios->nombre,
                      ['controller' => 'Negocios', 'action' => 'view', $negocios->id] ,
                      array('class' => 'TituloGrid')) ?>
                      <br />

                  </td>


              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>

        <?php endif; ?>
    </div>
</div>
