<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Catalogo Local';
?>
<!DOCTYPE html>
<html>

<head>

    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>



</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">

      <div class="top-bar-section">
          <ul class="left menu-izquierdo logo-header">
            <img width="160" height="160" src="http://localhost:8080/wordpress/wp-content/uploads/2020/11/LOGON.png" class="imgHeader" alt="Logo de CATLOC">


          </ul>

          <ul class="right icono-busqueda">
            <li><a class="menus-header" target="_self" href="http://localhost:8765">Home</a></li>
            <li class="dropdown-header">

                <a class="dropbtn menus-header">Categorías
                <div class="dropdown-content">
                  <a class="enlaces-categorias" href="http://localhost:8765/categorias/view/1">Comida</a>
                  <a class="enlaces-categorias" href="http://localhost:8765/categorias/view/8">Salud</a>
                  <a class="enlaces-categorias" href="http://localhost:8765/categorias/view/9">Belleza</a>
                  <a class="enlaces-categorias" href="http://localhost:8765/categorias/view/10">Educación</a>
                  <a class="enlaces-categorias" href="http://localhost:8765/categorias/view/11">Hospedaje</a>
                </div>
              </a>


            </li>
            


          </ul>
      </div>
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <h1><a href=""><?= $this->fetch('title') ?></a></h1>
            </li>
        </ul>

    </nav>
    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>

      <nav class="top-bar expanded" data-topbar role="navigation">

        <div class="top-bar-section">
            <ul class="left menu-izquierdo logo-header">
              <img width="160" height="160" src="http://localhost:8080/wordpress/wp-content/uploads/2020/11/LOGON.png" class="imgHeader" alt="LogoSitio: Gato azul cat loc">


            </ul>

            <ul class="right icono-busqueda">
              <li><a class="menus-header" target="_self" href="http://localhost:8765">Home</a></li>
              <li><a class="menus-header" target="_self" href="http://localhost:8765/Categorias">Categorias</a></li>
              <li><a class="menus-header" target="_blank" href="#">Promociones</a></li>

            </ul>


        </div>
          <ul class="title-area large-3 medium-4 columns">
              <li class="name">
                  <h1><a href=""><?= $this->fetch('title') ?></a></h1>
              </li>
          </ul>

      </nav>
      <p style="text-align: center; color: #376b8c;">© 2020 CatLoc | Todos los derechos reservados.<br>
Política de Privacidad</p>
    </footer>
</body>


</html>
