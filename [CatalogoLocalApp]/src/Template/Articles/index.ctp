<!-- File: src/Template/Articles/index.ctp -->
<div class="row">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <h1>Negocios</h1>
    <p><?= $this->Html->link("Añadir negocio", ['action' => 'add']) ?></p>
    <table>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Created</th>
            <th>Action</th>
        </tr>

    <!-- Aquí es donde iteramos nuestro objeto de consulta $articles, mostrando en pantalla la información del artículo -->

    <?php foreach ($articles as $article): ?>
        <tr>
            <td><?= $article->id ?></td>
            <td>
                <?= $this->Html->link($article->title, ['action' => 'view', $article->id]) ?>
            </td>
            <td>
                <?= $article->created->format(DATE_RFC850) ?>
            </td>
            <td>
                <?= $this->Form->postLink(
                    'Eliminar',
                    ['action' => 'delete', $article->id],
                    ['confirm' => '¿Estás seguro?'])
                ?>
                <?= $this->Html->link('Editar', ['action' => 'edit', $article->id]) ?>
            </td>
        </tr>
    <?php endforeach; ?>

    </table>



  </div>
  <div class="col-md-4"></div>
</div>
