<!-- File: src/Template/Negocios/add.ctp -->
<div class="row container-registrarnegocio">
  <div class="col-md-4">&nbsp;&nbsp;</div>
  <div class="col-md-4 registro-negocio">
<h1 class="TitulosForm">Añadir Negocio</h1>
<?php

    echo $this->Form->create($negocio, ['type' => 'file']);


    echo $this->Form->input('nombre', array('maxlength'=>'70'));
    echo $this->Form->input('direccion', array('maxlength'=>'150', 'placeholder'=>'4404  James Street, Rochester', 'rows' => '3'));
    echo $this->Form->input('horario', array('maxlength'=>'70', 'placeholder'=>'Lunes - Viernes 09:00 a.m. - 7:00 p.m.'));
    echo $this->Form->input('telefono', array('rule' => array('telefono', '/^[0-9]( ?[0-9]){8} ?[0-9]$/'), 'message' => 'Este campo solo admite numeros', 'maxlength'=>'10', 'placeholder'=>'2288101010'));

    echo $this->Form->input('ubicacion');
    echo $this->Form->input('categoria_id', [
        'options' => ['1' => 'Comida', '8' => 'Salud', '9' => 'Belleza', '10' => 'Educación', '11' => 'Hospedaje']
    ]);
    echo $this->Form->input('logo', ['type' => 'file']);
    //echo $this->Form->input('logo', ['type' => 'file']);
    echo $this->Form->button(__('Guardar Negocio'));
    echo $this->Form->end();
?>
  </div>

  <div class="col-md-4">&nbsp;&nbsp;</div>
</div>
