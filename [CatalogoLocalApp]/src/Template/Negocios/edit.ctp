<!-- File: src/Template/Negocios/edit.ctp -->
<div class="row container-registrarnegocio">
  <div class="col-md-4">&nbsp;&nbsp;</div>
  <div class="col-md-4 contenido-negocio">
<h1>Editar Negocio</h1>
<?php
    echo $this->Form->create($negocio, ['type' => 'file']);
    echo $this->Form->input('nombre');
    echo $this->Form->input('direccion', ['rows' => '3']);
    echo $this->Form->input('horario');
    echo $this->Form->input('telefono');
    
    echo $this->Form->input('ubicacion');
    echo $this->Form->input('categoria_id', [
        'options' => ['1' => 'Comida', '8' => 'Salud', '9' => 'Belleza', '10' => 'Educación', '11' => 'Hospedaje']
    ]);
    echo $this->Form->input('logo', ['type' => 'file']);
    echo $this->Form->button(__('Guardar Negocio'));
    echo $this->Form->end();
?>
</div>

<div class="col-md-4">&nbsp;&nbsp;</div>
</div>
