<!-- File: /src/Template/Negocios/index.ctp -->
<?php
/*
 * @var \App\View\AppView $this
 *@var \App\Model\Entity\Categoria[]|\Cake\Collection\CollectionInterface $categorias
 */
?>
<head>
  <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
  <?= $this->Html->meta('icon') ?>
  <script type="text/javascript">
    function openForm() {
     document.getElementById("formPrincipalBusq").style.display = "block";
    }

    function closeForm() {
     document.getElementById("formPrincipalBusq").style.display = "none";
    }
  </script>
  <script type="text/javascript">
    function validarForm(formulario)
    {
      if(formulario.palabra.value.length==0)
      { //¿Tiene 0 caracteres?
          formulario.palabra.focus();  // Damos el foco al control
          alert('Debes rellenar este campo'); //Mostramos el mensaje
          return false;
       } //devolvemos el foco
       return true; //Si ha llegado hasta aquí, es que todo es correcto
     }
   </script>
  <script type="text/javascript">
    function myFunction() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("buscadorNegocio");
      filter = input.value.toUpperCase();
      table = document.getElementById("tablaNegocios");
      tr = table.getElementsByTagName("tr");



      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
    </script>

  <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.3.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>


  <script type="text/javascript">
  $(document).ready(function() {
    getPagination('#tablaNegocios');

    function getPagination(table) {
      var lastPage = 1;

      $('#maxRows')
      .on('change', function(evt) {

        lastPage = 1;
        $('.pagination')
        .find('li')
        .slice(1, -1)
        .remove();
        var trnum = 0; // reset tr counter
        var maxRows = parseInt($(this).val()); // get Max Rows from select option

        if (maxRows == 5000) {
          $('.pagination').hide();
        } else {
          $('.pagination').show();
        }

        var totalRows = $(table).length; // numbers of rows
        $(table + ' tr:gt(0)').each(function() {
          // each TR in  table and not the header
          trnum++; // Start Counter
          if (trnum > maxRows) {
            // if tr number gt maxRows
            $(this).hide(); // fade it out
          }
          if (trnum <= maxRows) {
            $(this).show();
          } // else fade in Important in case if it ..
        }); //  was fade out to fade it in
        if (totalRows > maxRows) {
          // if tr total rows gt max rows option
          var pagenum = Math.ceil(totalRows / maxRows); // ceil total(rows/maxrows) to get ..
          //	numbers of pages
          for (var i = 1; i <= pagenum; ) {
            // for each page append pagination li
            $('.pagination #prev')
            .before(
              '<li data-page="' +
              i +
              '">\
              <span>' +
              i++ +
              '<span class="sr-only">(current)</span></span>\
              </li>'
            )
            .show();
          } // end for i
        } // end if row count > max rows
        $('.pagination [data-page="1"]').addClass('active'); // add active class to the first li
        $('.pagination li').on('click', function(evt) {
          // on click each page
          evt.stopImmediatePropagation();
          evt.preventDefault();
          var pageNum = $(this).attr('data-page'); // get it's number

          var maxRows = parseInt($('#maxRows').val()); // get Max Rows from select option

          if (pageNum == 'prev') {
            if (lastPage == 1) {
              return;
            }
            pageNum = --lastPage;
          }
          if (pageNum == 'next') {
            if (lastPage == $('.pagination li').length - 2) {
              return;
            }
            pageNum = ++lastPage;
          }

          lastPage = pageNum;
          var trIndex = 0; // reset tr counter
          $('.pagination li').removeClass('active'); // remove active class from all li
          $('.pagination [data-page="' + lastPage + '"]').addClass('active'); // add active class to the clicked
          // $(this).addClass('active');					// add active class to the clicked
          limitPagging();
          $(table).each(function() {
            // each tr in table not the header
            trIndex++; // tr index counter
            // if tr index gt maxRows*pageNum or lt maxRows*pageNum-maxRows fade if out
            if (
              trIndex > maxRows * pageNum ||
              trIndex <= maxRows * pageNum - maxRows
            ) {
              $(this).hide();
            } else {
              $(this).show();
            } //else fade in
          }); // end of for each tr in table
        }); // end of on click pagination list
        limitPagging();
      })
      .val(3)
      .change();

      // end of on select change

      // END OF PAGINATION
    }

    function limitPagging(){
      // alert($('.pagination li').length)

      if($('.pagination li').length > 7 ){
        if( $('.pagination li.active').attr('data-page') <= 3 ){
          $('.pagination li:gt(5)').hide();
          $('.pagination li:lt(5)').show();
          $('.pagination [data-page="next"]').show();
        }
        if ($('.pagination li.active').attr('data-page') > 3){
          $('.pagination li:gt(0)').hide();
          $('.pagination [data-page="next"]').show();
          for( let i = ( parseInt($('.pagination li.active').attr('data-page'))  -2 )  ; i <= ( parseInt($('.pagination li.active').attr('data-page'))  + 2 ) ; i++ ){
            $('.pagination [data-page="'+i+'"]').show();

          }

        }
      }
    }

    /*$(function() {
      // Just to append id number for each row
      $('table tr:eq(0)').prepend('<th> ID </th>');

      var id = 0;

      $('table tr:gt(0)').each(function() {
        id++;
        $(this).prepend('<td>' + id + '</td>');
      });
    });*/


  });


    </script>
</head>
<div class="row">
  <div class="buscador-principal">
    <h1 class="tituloBuscador"> Bienvenido a CATLOC ¿Qué negocio buscas? </h1>
    <div class="form-busqueda" id="formPrincipalBusq">

      <?= $this->Form->create($negocios, ['type' => 'get']) ?>
      <fieldset class="form-busqueda-home">
        <input type="text" id="buscadorNegocio" class="TextoFormBusqueda" onkeyup="myFunction()" placeholder="Buscar Negocio">
          <?php
              //echo $this->Form->control('NegocioSearch', ['label' => '', 'placeholder' => 'Buscar Negocio', 'class' => 'TextoFormBusqueda',  'onkeyup' => 'myFunction()']);

          ?>
          <?= $this->Form->button('<i class="icon-search"></i> ' .__(''), ['class'=>'buscar-btn', 'controller' => 'Negocios', 'action' => 'search']) ?>

      </fieldset>
      <?= $this->Form->end() ?>



    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-4"></div>
  <div class="col-md-4 columna-central">
<h1 id="TitulosNegocioHome">Negocios</h1>



      <div class="form-group"> 	<!--		Show Numbers Of Rows 		-->
        <select class  ="form-control" name="state" id="maxRows">
           <option value="5000">Mostrar todos</option>
           <option value="3" selected>4</option>
           <option value="10">10</option>
           <option value="15">15</option>
           <option value="20">20</option>
           <option value="50">50</option>
           <option value="70">70</option>
           <option value="100">100</option>
          </select>

        </div>


<table class="table table-striped table-class tabla-Negocios" id="tablaNegocios">

<tbody>
    <?php foreach ($negocios as $negocio): ?>

    <tr>

        <td>

            <img class="ImagenGrid" src="http://localhost:8080/catalogo-publicidad-web/[CatalogoLocalApp]/webroot/files/Negocios/logo/<?= h($negocio->logo) ?>" alt="Imagen Negocio">
            <br />
            <?= $this->Html->link($negocio->nombre,
            ['controller' => 'Negocios', 'action' => 'view', $negocio->id] ,
            array('class' => 'TituloGrid')) ?>
            <br />
            
        </td>


    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<!--
    <div class='pagination-container' >
				<nav>
				  <ul class="pagination">

            <li data-page="prev" >
								     <span> < <span class="sr-only">(current)</span></span>
								    </li>

        <li data-page="next" id="prev">
								       <span> > <span class="sr-only">(current)</span></span>
								    </li>
				  </ul>
				</nav>
			</div>

</div>

<div class="col-md-4 columna-central">
  <h1 class="TitulosHome">Categorias</h1>
</div>
</div>

<div class="gridCategorias">
    <?php
    // File login.ctp
    //$this->Element('../Template/Categorias/index.ctp'); // ('..' . DS . 'Users' . DS . 'register.ctp')
    foreach ($negocios as $negocio): ?>

    <div class="elementoGridCategoria">
      <?= $this->Html->link($negocio->nombre,
      ['controller' => 'Negocios', 'action' => 'view', $negocio->id] ,
      array('class' => 'TituloGrid')) ?>

      <br />

      <br />

    </div>
  <?php endforeach; ?>
</div>
-->

<div class="row">
  <div class="col-md-4"></div>
  <div class="col-md-4 columna-central">
    <h1 class="TitulosHome">Eventos</h1>
    <div class="gridEventos">

        <img class="ImagenEvento" src="http://localhost:8080/wordpress/wp-content/uploads/2020/11/evento2.jpeg" alt="Imagen Evento">

        <div class="elementoGrid">
          <img class="ImagenEvento" src="http://localhost:8080/wordpress/wp-content/uploads/2020/11/evento1.jpeg" alt="Imagen Evento">

        </div>
    </div>
  </div>
</div>





<div  id="mc_embed_signup" class="wrap-contact2">
			<form action="https://hotmail.us7.list-manage.com/subscribe/post?u=ada434c7e223a2b981ac52a2e&amp;id=36f1ff5225" class="contact-form" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate>
					<h3 class="titulo-form">Suscríbete</h3>

          <h3 class="mensaje-form">Recibe las mejores noticias de eventos y promociones.</h3>
          <br />

          <label class="form-group" id="mc_embed_signup_scroll">

            <input type="email" value="" name="form-control" class="form-control" id="mce-EMAIL" placeholder="Email" required>
            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ada434c7e223a2b981ac52a2e_36f1ff5225" tabindex="-1" value=""></div>

            <button type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn-form">Suscribirse<i class="zmdi zmdi-arrow-right"></i></button>
          </label>

				</form>
</div>
