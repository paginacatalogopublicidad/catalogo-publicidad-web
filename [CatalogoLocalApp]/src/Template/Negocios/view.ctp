<!-- File: /src/Template/Negocios/view.ctp -->

<div class="row container-negocio">
  <div class="col-md-4">&nbsp;&nbsp;</div>
  <div class="col-md-4 contenido-negocio">

  <h1 class="TitulosNegocio"><?= h($negocio->nombre) ?></h1>
    <div class="EstructuraNegocio">

      <p class="InformacionNegocio"><?= h($negocio->direccion) ?> <br />
      <?= h($negocio->horario) ?> <br /> <br /> &nbsp;
      <i class="icon-phone"></i>
      <?= h($negocio->telefono) ?></p>
      <?php

      $this->set('negocio', $negocio);

      //echo $this->Html->link('' . $negocio->logo_dir . '' . $negocio->logo);
      ?>
      <img class="ImagenNegocio" src="http://localhost:8080/catalogo-publicidad-web/[CatalogoLocalApp]/webroot/files/Negocios/logo/<?= h($negocio->logo) ?>" alt="Imagen Negocio">

    </div>

  <!-- <img class="ImagenNegocio" src="http://localhost:8080/wordpress/wp-content/uploads/2020/11/<?= h($negocio->logo) ?>" alt="Imagen Negocio"> -->

  </div>
  <div class="col-md-4 contenedor-mapa">&nbsp;
    <titulos id="TituloMapa">Mapa</titulos><br />
    &nbsp;
    <iframe id="MapaNegocio" src="<?= h($negocio->ubicacion) ?>"frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

  </div>


</div>
