**Metodología de desarrollo seleccionada**

OOHDM es la metodología de desarrollo que se seleccionó para el desarrollo del proyecto web catalogo de publicidad. Es una metodología  propuesta por Rossi y Schwabe (ROSSI 1996) para la elaboración de aplicaciones multimedia y tiene como objetivo simplificar y a la vez hacer más eficaz el diseño de aplicaciones hipermedia. 

En la primera etapa (Obtención de requerimientos) la salida es una lista de requerimientos y los casos de uso.

En la segunda etapa (Diseño Conceptual) la salida esperada es un diagrama de clases.

En la tercera etapa (Diseño Navegacional) se obtiene el diagrama de interfaz abstracta, puede ser más de uno.

En la cuarta etapa (Diseño de interfaz abstracta) un diagrama navegacional por cada diagrama de interfaz abstracta.

Por último(implementación), se obtiene la página implementada con la tecnología.
 


**Primera etapa: Obtención de requerimientos**

**Lista de requerimientos del catálogo de publicidad**

**RF-01**: El usuario del sitio deberá poder ver una lista de negocios registrados

**RF-02:** El usuario del sitio deberá poder filtrar la búsqueda de negocios por categoría.

**RF-03:** El usuario del sitio deberá poder visualizar la ficha de un negocio registrado en la página.

**RF-04:** El usuario del sitio deberá poder visualizar una lista de eventos próximos

**RF-05:** El usuario deberá poder visualizar los medicamentos o insumos que tienen las farmacias registradas

**RF-06:** El administrador deberá poder registrar, eliminar y editar nuevos negocios.